package com.bg.frienddeathapi.repository;

import com.bg.frienddeathapi.entity.Trade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TradeRepository extends JpaRepository<Trade, Long> {
}
