package com.bg.frienddeathapi.repository;

import com.bg.frienddeathapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
