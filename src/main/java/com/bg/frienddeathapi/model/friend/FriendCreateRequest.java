package com.bg.frienddeathapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCreateRequest {
    private String friendName;
    private String birthday;
    private String phoneNumber;
    private String etcMemo;

}
