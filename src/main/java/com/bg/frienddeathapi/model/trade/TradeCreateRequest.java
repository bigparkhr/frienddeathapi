package com.bg.frienddeathapi.model.trade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradeCreateRequest {
    private String type;
    private String category;
    private Double money;
    private String tradeDate;

}
