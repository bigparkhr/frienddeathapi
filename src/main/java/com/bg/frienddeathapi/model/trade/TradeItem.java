package com.bg.frienddeathapi.model.trade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradeItem {
    private Long friendId;
    private String friendName;
    private String birthday;
    private String type;
}
