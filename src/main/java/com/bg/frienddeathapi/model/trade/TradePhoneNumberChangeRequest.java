package com.bg.frienddeathapi.model.trade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradePhoneNumberChangeRequest {
    private String phoneNumber;
}
