package com.bg.frienddeathapi.service;

import com.bg.frienddeathapi.entity.Friend;
import com.bg.frienddeathapi.model.friend.FriendCreateRequest;
import com.bg.frienddeathapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id) {
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setFriendName(request.getFriendName());
        addData.setBirthday(request.getBirthday());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        friendRepository.save(addData);
    }

}
