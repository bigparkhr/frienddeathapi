package com.bg.frienddeathapi.service;

import com.bg.frienddeathapi.entity.Friend;
import com.bg.frienddeathapi.entity.Trade;
import com.bg.frienddeathapi.model.trade.TradeCreateRequest;
import com.bg.frienddeathapi.model.trade.TradeItem;
import com.bg.frienddeathapi.model.trade.TradePhoneNumberChangeRequest;
import com.bg.frienddeathapi.repository.TradeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TradeService {
    private final TradeRepository tradeRepository;

    public void setTrade(Friend friend, TradeCreateRequest request) {
        Trade addData = new Trade();
        addData.setFriend(friend);
        addData.setType(request.getType());
        addData.setCategory(request.getCategory());
        addData.setMoney(request.getMoney());
        addData.setTradeDate(request.getTradeDate());

        tradeRepository.save(addData);
    }

    public List<TradeItem> getTrades() {
        List<Trade> originList = tradeRepository.findAll();

        List<TradeItem> result = new LinkedList<>();

        for (Trade trade: originList) {
            TradeItem addItem = new TradeItem();
            addItem.setFriendId(trade.getFriend().getId());
            addItem.setFriendName(trade.getFriend().getFriendName());
            addItem.setBirthday(trade.getFriend().getBirthday());
            addItem.setType(trade.getType());

            result.add(addItem);
        }
        return result;
    }

    public void putPhoneNumber(long id, TradePhoneNumberChangeRequest request) {
        Trade originData = tradeRepository.findById(id).orElseThrow();
        originData.setTradeDate(request.getPhoneNumber());

        tradeRepository.save(originData);
    }
}
