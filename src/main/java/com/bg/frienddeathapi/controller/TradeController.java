package com.bg.frienddeathapi.controller;

import com.bg.frienddeathapi.entity.Friend;
import com.bg.frienddeathapi.model.trade.TradeCreateRequest;
import com.bg.frienddeathapi.model.trade.TradeItem;
import com.bg.frienddeathapi.model.trade.TradePhoneNumberChangeRequest;
import com.bg.frienddeathapi.service.FriendService;
import com.bg.frienddeathapi.service.TradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trade")
public class TradeController {
    private final FriendService friendService;
    private final TradeService tradeService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setFriend(@PathVariable long friendId, TradeCreateRequest request) {
        Friend friend = friendService.getData(friendId);
        tradeService.setTrade(friend, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<TradeItem> getTrade() {
        return tradeService.getTrades();
    }
    @PutMapping("/phone-number/trade-id/{tradeId}")
    public String putPhoneNumber (@PathVariable long tradeId, @RequestBody TradePhoneNumberChangeRequest request) {
        tradeService.putPhoneNumber(tradeId, request);

        return "OK";
    }
}
