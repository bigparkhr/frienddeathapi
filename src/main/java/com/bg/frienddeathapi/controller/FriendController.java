package com.bg.frienddeathapi.controller;

import com.bg.frienddeathapi.model.friend.FriendCreateRequest;
import com.bg.frienddeathapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendCreateRequest request) {
        friendService.setFriend(request);

        return "Ok";
    }
}
